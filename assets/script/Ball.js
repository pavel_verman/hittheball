cc.Class({
    extends: cc.Component,

    properties: {
        speed: 0,  // in seconds needed for 100 pixels
        hp: 0,
        points: 0,
    },

    setFlyAction: function () {
        // you can specify the direction (vectorX, vectorY). Default: UP
        var vectorX = 0;
        var vectorY = 100;
        var flyDirection = cc.moveBy(this.speed, cc.p(vectorX, vectorY));
        return cc.repeatForever(flyDirection);
    },

    isFlewAway: function () {
        if (this.node.name === 'emptyDefaultBall2') {
            this.game.missedBalls += 1;
            this.game.updateMissedBallsStat();
        }
        this.game.enemyPool.put(this.node);
    },

    setClickedEvent: function () {
        var self = this;
        cc.eventManager.addListener({
                event: cc.EventListener.TOUCH_ONE_BY_ONE,
                onTouchBegan: function(touch, event){
                    //var target = event.getCurrentTarget();
                    var locationInNode = self.convertPOS(touch.getLocation());
                    var ballLocationInNode = self.node.position;
                    //console.log(JSON.stringify(slf)) TypeError: Converting circular structure to JSON
                    //console.log('BUM : ' + JSON.stringify(locationInNode));
                    //console.log('My coord : ' + JSON.stringify(ballLocationInNode));
                    //var targetSize = target.getContentSize();
                    //var targetRectangle = cc.rect(self.node.position.x, self.node.position.y, targetSize.width, targetSize.height);
                    //console.log(JSON.stringify(targetSize));
                    var dist = cc.pDistance(ballLocationInNode, locationInNode);
                    if (dist <= self.node.width)
                    {
                        //console.log('HIT');
                        self.game.player.addPoints(self.points);
                        self.game.enemyPool.put(self.node);
                    }


                    //self.shotTaken(locationInNode);
                    //event.stopPropagation();
                    //if (0 === event.getButton()){
                    // left click
                    //self.game.enemyPool.put(self.node);
                    //cc.eventManager.dispatchEvent(event);
                    //}
                },
            }
            , self.node);
    },

    convertPOS: function(point) {

        return cc.p(point.x - this.game.node.width/2, point.y - this.game.node.height/2);

    },

    // use this for initialization
    onLoad: function () {
        // initialize flying action
        //this.node.setAnchorPoint(cc.p(0.5,0.5));
        this.flying = this.setFlyAction();
        this.node.runAction(this.flying);
        this.setClickedEvent()
    },

    // called every frame, uncomment this function to activate update callback
     update: function (dt) {
        if (this.game.isGameOver){
            this.node.destroy();
        }
        if (this.node.position.y >= this.game.node.height/3){
            this.isFlewAway();
        }

    },
});

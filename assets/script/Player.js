cc.Class({
    extends: cc.Component,

    properties: {
        playerName: '',
        score: 0,
        scoreDisplay: {
            default: null,
            type: cc.Label
        },
        playerNameDisplay: {
            default: null,
            type: cc.Label
        }
    },

    addPoints: function (points) {
        this.score += points;
        this.scoreDisplay.string = 'Score: ' + this.score.toString();
    },

    setNewPlayerName: function (newPlayerName) {
        this.playerName = newPlayerName;
        this.playerNameDisplay.string = 'Player: ' + this.playerName.toString();
    },

    startPlaying: function (playerName) {
        this.setNewPlayerName(playerName);
        this.addPoints(0);
    },

    gameOver: function () {
        this.scoreDisplay.string = '';
        this.playerNameDisplay.string = '';
        return this.score;
    },

    // use this for initialization
    onLoad: function () {
        this.scoreDisplay.string = '';
        this.playerNameDisplay.string = '';
    },

    // called every frame, uncomment this function to activate update callback
    // update: function (dt) {

    // },
});

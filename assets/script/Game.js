//const Player = require('Player');

cc.Class({
    extends: cc.Component,

    properties: {
        //player: {
        //    default: null,
        //    type: cc.Any
        //},
        playerNameEntry: {
            default: null,
            type: cc.EditBox
        },
        // this property quotes the PreFab resource of balls
        ballPrefab: {
            default: null,
            type: cc.Prefab
        },
        badBallPrefab: {
            default: null,
            type: cc.Prefab
        },
        badBollsCount: 0,
        // display result score
        missedBallsDisplay: {
            default: null,
            type: cc.Label
        },
        totalScoreDisplay: {
            default: null,
            type: cc.Label
        },
        scoreTableDisplay: {
            default: null,
            type: cc.RichText
        },
        // weight of the ball
        maxMissedBalls: 0,
        generationPeriod: 0,  // seconds
        maxInScoreTable: 0,
    },

    generateSpawnWave: function () {

        var ballPosition;
        for (var curBallPosition = 0; curBallPosition < this.ballsPositionCounts; curBallPosition++) {
            // generate a new node in the scene with a preset template
            var newBall = this.createBall();
            if (newBall) {
                // put the newly added node under the Canvas node
                //this.node.addChild(newBall);
                // make a link to the parent
                newBall.getComponent('Ball').game = this;
                // set up a random position for the ball on the start line
                ballPosition = this.generatePositionForNewBall(newBall, curBallPosition);
                if (ballPosition) {
                    newBall.setPosition(ballPosition);
                }
                else {
                    // new ball generated not in the scene
                    this.enemyPool.put(newBall);
                }
            }
        }
    },

    createBall: function () {

        return this.createDefaultBall();
    },

    createDefaultBall: function () {
        var ball = null;
        if (this.enemyPool.size() > 0) { // use size method to check if there're nodes available in the pool
            ball = this.enemyPool.get();
        } else { // if not enough node in the pool, we call cc.instantiate to create node
            ball = cc.instantiate(this.ballPrefab);
        }
        ball.parent = this.node; // add new enemy node under the Canvas node
        //enemy.getComponent('Ball').init(); //initialize enemy. Why this don't work?
        ball.init(); //initialize enemy
        if (ball.name === 'emptyDefaultBall2') {
            var red = Math.ceil(cc.random0To1() * 255.0);
            var green = Math.ceil(cc.random0To1() * 255.0);
            var blue = Math.ceil(cc.random0To1() * 255.0);
            //console.log(red.toString() + green.toString() + blue.toString());
            ball.color = cc.color(red, green, blue);
        }
        return ball;
    },

    generatePositionForNewBall: function (ballInstance, curBallPosition){
        var startSpawnVerticalPosition = -this.node.width/2 + ballInstance.width;
        var randX = cc.randomMinus1To1() * ((this.node.width/this.ballsPositionCounts)/2 - ballInstance.width/2)
            + curBallPosition * (this.node.width/this.ballsPositionCounts)
            + startSpawnVerticalPosition;
        if (randX < -this.node.width/2 + ballInstance.width) {
            return null;
        }
        if (randX > this.node.width/2 - ballInstance.width) {
            return null;
        }
        var randY = cc.randomMinus1To1() * ballInstance.height  - this.node.height;
        return cc.p(randX, randY);
    },

    startWaveGeneration: function () {
        return cc.repeatForever(cc.sequence(
            cc.callFunc(this.generateSpawnWave, this),
            cc.delayTime(this.generationPeriod)));
    },

    createBadBall: function () {
        var badBall = cc.instantiate(this.badBallPrefab); // create node instance
        this.enemyPool.put(badBall); // populate your pool with putInPool method
    },

    startGame: function () {
        //console.log(JSON.stringify(this.playerNameEntry));
        //console.log(typeof this.playerNameEntry.getText());
        //console.log(typeof this.playerNameEntry.string);
        this.player.startPlaying(this.playerNameEntry.string);
        this.playerNameEntry.destroy();
        this.updateMissedBallsStat();
        this.swapning = this.startWaveGeneration();
        this.node.runAction(this.swapning);
    },

    updateMissedBallsStat: function () {
        var curHP = this.maxMissedBalls - this.missedBalls;
        if (curHP < 0){
            curHP = 0;
        }
        if (!this.isGameOver)
            this.missedBallsDisplay.string = 'HP: ' + curHP.toString() ;
        else
            this.missedBallsDisplay.string = '';
    },

    gameOver: function () {
        //this.stopAllActions();
        //cc.director.loadScene('resultField')
        this.node.stopAllActions();
        this.isGameOver = true;
        var score = this.player.gameOver();
        this.missedBallsDisplay.string = '';
        this.saveScore(score);
        this.showScore(score);
    },

    saveScore: function (score) {
        var scoreTable = JSON.parse(cc.sys.localStorage.getItem(JSON.stringify('score')));
        var playerName = this.player.playerName;
        var newScoreRecord = [playerName, score];
        if (!scoreTable) {
            console.log('new score table');
            scoreTable = [newScoreRecord];
        }
        else{
            // insert new record
            scoreTable.push(newScoreRecord);
            // sort by second value == score
            scoreTable.sort(function(first, second) {
                return second[1] - first[1];
            });
            scoreTable = scoreTable.slice(0, this.maxInScoreTable);
        }
        cc.sys.localStorage.setItem(JSON.stringify('score'), JSON.stringify(scoreTable));
    },

    showScore: function (score) {
        //var scoreLabel = cc.Label.createWithSystemFont('You score:' + score.toString(),  'Times New Roman',  32,  cc.size(320, 32),  cc.TextAlignment.CENTER);
        this.totalScoreDisplay.string = 'You score: ' + score.toString();
        var scoreTable = JSON.parse(cc.sys.localStorage.getItem(JSON.stringify('score')));
        console.log(JSON.stringify(scoreTable));
        this.scoreTableDisplay.string = "<color=#ff0000><b> Total score </b></color><br/>";
        for (var i=0; i<scoreTable.length; i++) {
            this.scoreTableDisplay.string += scoreTable[i][0] + ' : ' + scoreTable[i][1] + '<br/>';
        }
    },

    // use this for initialization
    onLoad: function () {
        // TODO add another game mod: with Timer
        // TODO resize objects on canvas when rotated
        this.missedBallsDisplay.string = '';
        this.isGameOver = false;
        this.player = this.getComponent('Player');
        //this.player.playerName.node.setVisible(false);
        //this.player.playerNameDisplay.node.setVisible(false);
        //this.player.init();

        this.ballsPositionCounts = 10;  // max balls count per one wave generation
        this.missedBalls = 0;  // the count off missed balls.
        this.enemyPool = new cc.NodePool();
        var initCount = 20;
        for (var i = 0; i < initCount; i++) {
            var ball = cc.instantiate(this.ballPrefab); // create node instance
            this.enemyPool.put(ball); // populate your pool with putInPool method
        }

        var interval = this.generationPeriod * (Math.floor(cc.random0To1() * 3) + 1);
        var repeat = this.badBollsCount;
        var delay = this.generationPeriod * (Math.floor(cc.random0To1() * 3) + 1);
        console.log(interval, repeat, delay);
        this.schedule(this.createBadBall, interval, repeat, delay);

        //var sameDelay = cc.delayTime(this.generationPeriod * (Math.floor(Math.random() * 3) + 1));
        //var badBallCreating = cc.sequence();
        //for (i = 0; i < this.badBollsCount; i++) {
        //    badBallCreating.
        //    cc.callFunc(this.createBadBall(), this);
        //}

    },

    // called every frame, uncomment this function to activate update callback
     update: function (dt) {
         if ((this.missedBalls > this.maxMissedBalls)&&(!this.isGameOver)){
             this.gameOver();
             return;
         }
     },
});
